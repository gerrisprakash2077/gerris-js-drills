function defaults(obj,defaultProps){
    for(i in defaultProps){
        if(!(i in obj)){
            obj[i]=defaultProps[i];
        }
    }
    return obj;
}


module.exports=defaults;