function reduce(elements, cb,startingValue){
    let result=elements[startingValue];
    for(let index=startingValue+1;index<elements.length;index++){
        result=cb(result,elements[index]);
    }
    return result;
}


module.exports=reduce;