

function flatten(elements) {
    let result=[]
    rec(0,elements,result);
    return result;
}

function rec(index,elements,result){
    if(index>=elements.length){
        return;
    }

    if(Array.isArray(elements[index])){
        rec(0,elements[index],result);
    }
    else{
        result.push(elements[index]);
    }
    rec(index+1,elements,result);
}


module.exports=flatten;