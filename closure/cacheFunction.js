
function cacheFunction(cb) {

    let cache={initial:"northing"}
    return function(alphabet){
        if(cache[alphabet]){
            //console.log(cache[alphabet],"from cache",cache)
            
            return cache[alphabet]+" from cache";
        }
        else{
            cache[alphabet]=cb(alphabet)
            return  cache[alphabet]+" created and inserted to cache"
            //console.log(cache[alphabet], "created and inserted to cache",cache)
        }
        return -1
    }
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
}


module.exports=cacheFunction