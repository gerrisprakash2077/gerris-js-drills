function counterFactory(num) {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    const operations={
        increment:function(){
            return ++num;
        },
        decrement:function(){
            return --num;
        }
    }
    return operations;
}


module.exports=counterFactory;