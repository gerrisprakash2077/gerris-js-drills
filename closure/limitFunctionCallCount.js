function limitFunctionCallCount(cb, n) {
    let count=0;
    return function(){
        if(count++<n){
            cb()
        }
    }
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
}


module.exports=limitFunctionCallCount;