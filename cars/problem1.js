function findCarById(inventory,idToFind){
    return `Car ${idToFind} is a ${inventory[idToFind-1].car_year} ${inventory[idToFind-1].car_make} ${inventory[idToFind-1].car_model}`;
}


module.exports=findCarById;
