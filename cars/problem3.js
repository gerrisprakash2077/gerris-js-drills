function sortBasedOnCarModel(inv){
    for(let index1=0;index1<inv.length;index1++){
        for(let index2=index1+1;index2<inv.length;index2++){
            if(inv[index1].car_model>inv[index2].car_model){
                let temp=inv[index1];
                inv[index1]=inv[index2];
                inv[index2]=temp;
            }
        }
    }
    return inv;
}

module.exports=sortBasedOnCarModel;