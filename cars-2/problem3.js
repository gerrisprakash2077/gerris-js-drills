function sortBasedOnCarModel(inv){
    inv.sort((a,b)=>{
        if(a.car_model>b.car_model){
            return 1
        }
        else if(a.car_model<b.car_model){
            return -1;
        }
        else{
            return 0;
        }
    })
    return inv;
}

module.exports=sortBasedOnCarModel;