function findCarById(inventory,idToFind){
    let car=inventory.find((obj)=>{
        return obj.id==idToFind
    })
    return `Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`;
}

module.exports=findCarById;
