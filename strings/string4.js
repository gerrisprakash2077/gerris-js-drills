function nameFromObj(obj){
    let result="";
    if(obj["first_name"]){
        for(let index=0;index<obj["first_name"].length;index++){
            if(index===0){
                result+=obj["first_name"][index].toUpperCase();
            }
            else{
                result+=obj["first_name"][index].toLowerCase();
            }
        }
    }
    if(obj["middle_name"]){
        result+=" ";
        for(let index=0;index<obj["middle_name"].length;index++){
            if(index===0){
                result+=obj["middle_name"][index].toUpperCase();
            }
            else{
                result+=obj["middle_name"][index].toLowerCase();
            }
        }
    }
    if(obj["first_name"]){
        result+=" ";
        for(let index=0;index<obj["last_name"].length;index++){
            if(index===0){
                result+=obj["last_name"][index].toUpperCase();
            }
            else{
                result+=obj["last_name"][index].toLowerCase();
            }
        }
    }
    return result;
}


module.exports=nameFromObj;