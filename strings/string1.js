function dollarToNumeric(str){
    let result="";
    for(let index=0;index<str.length;index++){
        if(str[index]!="$" && str[index]!=","){
            result+=str[index];
        }
    }
    return Number(result);
}


module.exports=dollarToNumeric;