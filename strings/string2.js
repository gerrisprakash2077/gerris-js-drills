function ipDiaAssemble(ip){
    const arr=ip.split(".");
    for(let index=0;index<arr.length;index++){
        if(!Number(arr[index])){
            return [];
        }
        arr[index]=Number(arr[index]);
    }
    return arr;
}


module.exports=ipDiaAssemble