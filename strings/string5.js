function arrayToString(arr){
    let result="";
    for(let index=0;index<arr.length;index++){
        if(index!=arr.length-1){
            result+=arr[index]+" ";
        }
        else{
            result+=arr[index]+".";
        }
    }
    return result;
}


module.exports=arrayToString;